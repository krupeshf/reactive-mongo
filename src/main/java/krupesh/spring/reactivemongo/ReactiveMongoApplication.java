package krupesh.spring.reactivemongo;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.UUID;

// All components in single place - easy to read and understand :)
// When its actual project it should be in different packages and files

@SpringBootApplication
public class ReactiveMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveMongoApplication.class, args);
    }
}

@Component
class DataInitializer {
    private final LocationRepository locationRepository;

    @Autowired
    DataInitializer(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void run() {
        locationRepository.deleteAll()
                .subscribe();
//                .subscribe(null, null, () ->
//                        locationRepository.save(new Location().toBuilder().id(UUID.randomUUID().toString()).latitude(55D).build())
//                                .subscribe(null, null, ()
//                                        -> locationRepository.findAll().subscribe(System.out::println)));
    }
}


@Configuration
class ReactiveMongoConfiguration {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ReactiveMongoConfiguration.class);

    @Value("${api.key}")
    private String API_KEY;

    @Bean
    public WebClient webClient() {
        return WebClient
                .builder()
                .baseUrl("https://api.darksky.net/forecast/" + API_KEY)
                .filter(ExchangeFilterFunctions.basicAuthentication("clientId", "clientSecret"))
                .filter(logRequest())
                .build();
    }

    // This method returns filter function which will log request data
    private static ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            log.info("Request: {} {}", clientRequest.method(), clientRequest.url());
            clientRequest.headers().forEach((name, values) -> values.forEach(value -> log.info("{}={}", name, value)));
            return Mono.just(clientRequest);
        });
    }

}

@RestController
class RMController {

    private final RMService rmService;

    @Autowired
    public RMController(RMService service) {
        this.rmService = service;
    }

    @PostMapping("/loc/random")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Location> getRandomLocation() {
        return rmService.getRandomLocation();
    }

    @GetMapping("/loc")
    public Flux<Location> getAllLocations() {
        return rmService.getAllLocations();
    }

    @GetMapping("/loc/{id}")
    public Mono<Location> getLocation(@PathVariable String id) {
        return rmService.getLocation(id);
    }
}


@Service
class RMService {

    private final WebClient client;
    private final LocationRepository locationRepository;

    @Autowired
    public RMService(WebClient client, LocationRepository locationRepository) {
        this.client = client;
        this.locationRepository = locationRepository;
    }

    public Flux<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    public Mono<Location> getLocation(String id) {
        return locationRepository.findById(id);
    }

    public Mono<Location> getRandomLocation() {
        return client.get()
                .uri("/{latitude},{longitude}?exclude=minutely,hourly,daily,alerts,flags,offset", Utilities.getRandomLatitude(), Utilities.getRandomLongitude())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(JsonNode.class)
                .map(this::convertAndSaveCurrentData);
    }

    private Location convertDarkSkyJsonToClass(JsonNode jsonNode) {
        JsonNode currentNode = jsonNode.get("currently");
        return new Location()
                .toBuilder()
                .id(UUID.randomUUID().toString())
                .latitude(jsonNode.get("latitude").asDouble())
                .longitude(jsonNode.get("longitude").asDouble())
                .timezone(jsonNode.get("timezone").asText())
                .temperature(currentNode.get("temperature").asDouble())
                .humidity(currentNode.get("humidity").asDouble())
                .precipProbability(currentNode.get("precipProbability").asDouble())
                .time(new Date())
                .build();
    }


    private Location convertAndSaveCurrentData(JsonNode jsonNode) {
        Location location = convertDarkSkyJsonToClass(jsonNode);
        // by default it does update or insert
        // https://docs.mongodb.com/manual/reference/method/db.collection.save/
        // we have to give subscribe() or any other subscribe because then save does not execute :(
        // nothing happens until you subscribe
        // http://projectreactor.io/docs/core/release/reference/docs/index.html#reactive.subscribe
        locationRepository.save(location).subscribe().dispose();
//        locationRepository.save(location).subscribe(null, null, () -> this.locationRepository.findAll().subscribe(System.out::println));
        return location;
    }

}

interface LocationRepository extends ReactiveMongoRepository<Location, String> {
}

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
class Location {
    @Id
    private String id;
    private Double latitude;
    private Double longitude;
    private String timezone;
    private Double temperature;
    private Double humidity;
    private Double precipProbability;
    private Date time;
}

class Utilities {
    public static double getRandomDoubleBetweenRange(double min, double max) {
        return (Math.random() * ((max - min) + 1)) + min;
    }

    public static double getRandomLatitude() {
        return getRandomDoubleBetweenRange(-90, 90);
    }

    public static double getRandomLongitude() {
        return getRandomDoubleBetweenRange(-180, 180);
    }
}
