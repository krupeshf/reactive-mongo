package krupesh.spring.reactivemongo;

import lombok.extern.java.Log;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log
public class ReactiveMongoApplicationTests {

    @Autowired
    private LocationRepository locationRepository;

    private Location testLocation;

    private WebTestClient client;

    @Before
    public void setup() {
        testLocation = new Location()
                .toBuilder()
                .id(UUID.randomUUID().toString())
                .latitude(1D)
                .longitude(1D)
                .build();

        client = WebTestClient.bindToServer().baseUrl("http://localhost:8080").build();
    }

    @Test
    public void fullIntegration_oneOfTheLocationIsTestLocation() {
        locationRepository.save(testLocation).subscribe();
        client.get()
                .uri("/loc")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Location.class).contains(testLocation);
    }

    @Test
    public void fullIntegration_onlyLocationIsTestLocation() {
        locationRepository.deleteAll().subscribe(null, null, () ->
                locationRepository.save(testLocation).subscribe());
        client.get()
                .uri("/loc")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Location.class)
                .hasSize(1)
                .contains(testLocation);
    }

    @Test
    public void fullIntegration_getLocationById() {
        locationRepository.save(testLocation).subscribe();
        client.get()
                .uri("/loc/"+testLocation.getId())
                .exchange()
                .expectStatus().isOk()
                .expectBody(Location.class)
                .isEqualTo(testLocation);
    }

    @Test
    public void fullIntegration_getRandomLocation() {
        client.post()
                .uri("/loc/random")
                .exchange()
                .expectStatus().isCreated()
                .expectBody(Location.class);
    }

    @Test
    public void fullIntegration_invalidEndPoint() {
        client.get()
                .uri("someInvalidPoint")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

}
